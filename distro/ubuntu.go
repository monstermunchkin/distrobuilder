package distro

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"

	lxd "github.com/lxc/lxd/shared"
	"github.com/monstermunchkin/distrobuilder/shared"
)

type Ubuntu struct {
	Release string
	Arch    string
	Variant string
	rootfs  string
	fname   string
}

const ubuntuBaseURL = "cdimage.ubuntu.com/ubuntu-base/releases"

func (u *Ubuntu) Download() error {
	var (
		client   http.Client
		buffer   bytes.Buffer
		checksum string
	)

	u.fname = fmt.Sprintf("ubuntu-base-%s-base-%s.tar.gz", u.Release, u.Arch)

	f, err := os.Create(u.fname)
	if err != nil {
		return err
	}

	_, err = shared.DownloadFileSha256(&client, "", nil, nil, "",
		"http://"+path.Join(ubuntuBaseURL, u.Release, "release", "SHA256SUMS"),
		"", &buffer)
	if err != nil {
		return err
	}

	scanner := bufio.NewScanner(&buffer)
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), " ")
		if len(s) == 2 && s[1] == fmt.Sprintf("*ubuntu-base-%s-base-%s.tar.gz", u.Release, u.Arch) {
			checksum = s[0]
			break
		}
	}

	progress := func(progress shared.ProgressData) {
		fmt.Printf("%s\r", progress.Text)
	}

	_, err = shared.DownloadFileSha256(&client, "", progress, nil, u.fname,
		"http://"+path.Join(ubuntuBaseURL, u.Release, "release", u.fname),
		checksum, f)
	if err != nil {
		return err
	}
	fmt.Println("")

	return nil
}

func (u *Ubuntu) Unpack() error {
	var err error

	err = os.MkdirAll(u.rootfs, 0755)
	if err != nil {
		return err
	}

	if u.rootfs == "" {
		u.rootfs, err = ioutil.TempDir(os.TempDir(), "rootfs.")
		if err != nil {
			return err
		}
	}

	if err := shared.Unpack(u.fname, u.rootfs); err != nil {
		return err
	}

	return nil
}

func (u *Ubuntu) AddPackages(pkgs ...string) error {
	fhost, err := os.Open("/etc/resolv.conf")
	if err != nil {
		return err
	}

	// FIXME: make sure u.rootfs is set otherwise it'll overwrite the host's resolve.conf
	fchroot, err := os.Create(filepath.Join(u.rootfs, "/etc/resolv.conf"))
	if err != nil {
		return err
	}

	io.Copy(fchroot, fhost)
	fhost.Close()
	fchroot.Close()

	err = os.Setenv("SHELL", "/bin/bash")
	if err != nil {
		return err
	}
	err = os.Setenv("PATH", "/sbin:/bin:/usr/sbin:/usr/bin")
	if err != nil {
		return err
	}

	exit, err := shared.Chroot(u.rootfs)
	if err != nil {
		return err
	}
	defer exit()

	args := []string{"-c", "apt-get -y update"}
	out, err := lxd.RunCommand("/bin/sh", args...)
	if err != nil {
		log.Println(out)
		return err
	}

	args = []string{"-c", "apt-get", "-y", "install"}
	args = append(args, pkgs...)
	out, err = lxd.RunCommand("sh", args...)
	if err != nil {
		log.Println(out)
		return err
	}

	return nil
}

func (u *Ubuntu) RemovePackages(pkgs ...string) error {
	return nil
}
