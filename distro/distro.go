package distro

type Distro interface {
	Download() error
	Unpack() error
	AddPackages(...string) error
	RemovePackages(...string) error
}
