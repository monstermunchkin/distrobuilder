package main

import (
	"log"
	"os"

	"github.com/monstermunchkin/distrobuilder/distro"

	"gopkg.in/urfave/cli.v1"
	_ "gopkg.in/yaml.v2"
)

/*
* - download tarball
* - extract tarball
* - add/remove packages
* - do LXC/LXD specific changes
* - repack tarball / create squashfs
* - create LXC/LXD metadata
 */
func main() {
	app := cli.NewApp()
	app.Usage = "image generator"
	// INPUT can either be a file or '-' which reads from stdin
	//app.UsageText = "distrobuilder [global options] INPUT"
	app.ArgsUsage = "INPUT"
	app.HideHelp = true
	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:  "lxc",
			Usage: "generate LXC image files (rootfs + meta)",
		},
		cli.BoolFlag{
			Name:  "lxd",
			Usage: "generate LXD image files (rootfs + meta)",
		},
		cli.BoolFlag{
			Name:  "plain",
			Usage: "generate plain chroot",
		},
	}
	app.Action = func(c *cli.Context) error {
		var d distro.Distro
		d = &distro.Ubuntu{
			Release: "17.10",
			Arch:    "amd64",
		}
		if err := d.Download(); err != nil {
			log.Println(err)
			return err
		}

		if err := d.Unpack(); err != nil {
			log.Println("unpack:", err)
			return err
		}

		if err := d.AddPackages("neovim"); err != nil {
			log.Println("addpackages:", err)
			return err
		}

		return nil
	}
	app.Run(os.Args)
}
